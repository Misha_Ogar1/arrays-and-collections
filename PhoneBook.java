import java.util.Scanner;
import java.util.TreeMap;

public class PhoneBook {
    //метод main
    public static void main(String[] args) {

        //создаём коллекцию Map
        TreeMap<String, String> namesAndPhones = new TreeMap<>();

        //создаём возможность ввода
        Scanner scan = new Scanner(System.in);

        //создаём цикл, позволяющий выполнять программу, пока его условие true
        while (true) {

            //просим ввести имя
            System.out.println("Введите Имя: ");

            //создаём возможность ввода имени
            String name = scan.nextLine();

            //вывод Map если имя уже есть
            if (namesAndPhones.containsKey(name)) {
                System.out.println(namesAndPhones.get(name));
                continue;
            }

            //вывод Map после команды LIST
            if (name.equals("LIST")) {
                System.out.println(namesAndPhones);
                continue;
            }

            //просим ввести номер телефона
            System.out.println("Введите номер телефона: ");

            //создаём возможность ввода телефона
            String phone = scan.nextLine();

            //создаём регулярку для имени
            String regexName = "[a-zA-Z]+";

            //создаём регулярку для номера
            String regexPhone = "[0-9]+";

            //проверяем соответствие введённого имени и номера телефона, регулярке
            if (name.matches(regexName) && phone.matches((regexPhone))) {
                //выводим содержимое Map
                namesAndPhones.put(name, phone);
            } else {
                System.out.println("Вы ввели данные неверного формата! Рекомендуется исправить это");
            }
        }
    }
}
