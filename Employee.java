public class Employee {

    //создание переменной для имени
    String personal;

    //создание конструктора Employee
    public Employee(String person) {
        this.personal = person;
    }

    //создание метода toString для вывода информации в нужном формате
    public String toString() {
        return "name: " + personal;
    }
}
