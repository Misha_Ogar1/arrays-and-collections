import java.util.*;
public class Email {
    //создание списка HashSet
    static HashSet<String> words = new HashSet<>();

    //регулярка
    static String englishLetters = "[a-z]";
    static String regex = englishLetters + "+[@]{1}" + englishLetters + "+[.]{1}" + englishLetters + "+";

    //ввод email
    static Scanner scan = new Scanner(System.in);
    static String input = scan.nextLine();

    //метод main() для конечного вывода
    public static void main(String[] args) {
        if (input.contains("ADD")) {
            add();
        }
        list();
    }

    //метод add() с логикой
    public static void add() {

        //убираем слово ADD
        int space = input.indexOf(" ");
        input = input.substring(space).trim();

        //добавляем в список верный email
        if (input.matches(regex)) {
            words.add(input);
        } else {
            System.out.println("Проверьте корректность email!! и попробуйте заново");
        }
    }

    public static void list() {
        //цикл для вывода элементов списка words
        for (String word : words) {
            System.out.println(word);
        }
    }

}

