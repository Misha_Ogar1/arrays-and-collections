import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SortingAndSearchingItemsInCollections {

    //метод main
    public static void main(String[] args) {

        //создание ArrayList
        ArrayList<String> numberCar = new ArrayList<>();

        //сортировка ArrayList
        Collections.sort(numberCar);

        //пока условие истинно, программа выполняется
        while (true) {

            //возможность ввода с консоли
            Scanner vvod = new Scanner(System.in);

            //регулярка
            String regexLetters = "[CMTBAPOHEY]{2}[0-9]{3}[CMTBAPOHEY]{1}[0-9]{2,3}";

            //цикл, обеспечивающий постоянный ввод номера
            while (true) {

                //переменная для номера машины
                String number = vvod.nextLine();

                //вывод всех введённых номеров
                if (number.equals("LIST")) {
                    System.out.println(numberCar);

                    //создание переменной, показывающей количество наносекунд с данный момент
                    long start = System.nanoTime();

                    //бинарный поиск
                    int binary = Collections.binarySearch(numberCar, "CM123B12");
                    System.out.println(binary);

                    //создание и вывод переменной, показывающей сколько времени длилась вышенаписанная операция
                    long result = System.nanoTime() - start;
                    System.out.println(result);
                    continue;
                }

                //добавление в список номера, соответствующего регулярке
                if (number.matches(regexLetters)) {
                    numberCar.add(number);
                } else {
                    System.out.println("Введён неккоректный номер машины!");
                }
            }

        }
    }
}
