import java.util.HashMap;

public class MapTest {
    public static void main(String[] args) {

        //создание объекта Employee
        Employee harry = new Employee("");

        //создаём разновидность коллекции HashMap
        HashMap<String, Employee> staff = new HashMap<>();

        //заполняем staff
        staff.put("1", new Employee("Harry Hacker"));
        staff.put("2", new Employee("Amy Lee"));
        staff.put("3", new Employee("Gary Cooper"));

        //удаление элемента
        staff.remove("2");

        //вывести определённый объект, используя его ключ
        System.out.println(staff.get("3") + "\r\n");

        //перемена фамилии у определённого объекта
        staff.put("3", new Employee("Gary Miller"));

        //выводим содержимое staff
        for (String person : staff.keySet()) {
            System.out.println(person + " => " + staff.get(person));
        }
    }
}
