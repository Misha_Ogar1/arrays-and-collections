import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SearchAndSort {

    //метод main
    public static void main(String[] args) {

        //возможность ввода
        Scanner vvod = new Scanner(System.in);

        //создание списка ArrayList
        ArrayList<String> words = new ArrayList<>();

        //заполнение списка словами
        words.add("One");
        words.add("Two");
        words.add("Tree");
        words.add("Four");

        //выводим неотсортированный ArrayList
        for (String word : words) {
            System.out.println("Неотсортированный ArrayList: " + word);
        }

        //сортируем ArrayList
        Collections.sort(words);

        //вводим слово
        System.out.println("Введите слово: ");
        String strWord = vvod.nextLine();

        //ищем бинарным поиском в ArrayList
        int vseWords = Collections.binarySearch(words, strWord);
        System.out.println("\r\n" + "Если введённое значение содержится в ArrayList, то будет выведено \"0 or > 0 \" " + " => " + vseWords + "\r\n");

        //выводим содержимое отсортированного ArrayList
        for (String wordSort : words) {
            System.out.println("Отсортированный ArrayList: " + wordSort);
        }

        //перенос стоки
        System.out.println();

        //цикл, выводящий слово "YES strWord" если список содержит слово "strWord"
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).contains(strWord)) {
                System.out.println("YES: " + strWord + " : " + i + " => " + words.get(i));
            } else {
                System.out.println("null : " + i + " => " + words.get(i));
            }
        }
    }
}

