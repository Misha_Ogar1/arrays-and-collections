import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class HashMapAndTreeMap {
    //метод main
    public static void main(String[] args) {
        //Создаём TreeMap
        TreeMap<String, Integer> goodscount = new TreeMap<>();

        //Создаём возможность ввода товара
        Scanner scanner = new Scanner(System.in);

        //цикл добавляющий и выводящий информацию в goodscount
        while (true) {
            //Вводимая пользователем переменная
            String goodName = scanner.nextLine();

            //вывод товаров и их количества
            if (goodName.equals("LIST")) {
                printMap(goodscount);
                continue;
            }

            //Количество товара
            int count = 1;

            //При дублировании товара добавление его количества
            if (goodscount.containsKey(goodName)) {
                count = goodscount.get(goodName) + 1;
            }

            //Добавляем в goodscount товар и его количество
            goodscount.put(goodName, count);
        }
    }

    //цикл, выводящий товары и их количество
    private static void printMap(Map<String, Integer> map) {
        for (String key :
             map.keySet()) {
            System.out.println(key + " => " + map.get(key));
        }
    }
}
